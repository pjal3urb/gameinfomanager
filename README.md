General-purpose information gathering and distribution script(s) for use in other scripts. Core functionality is implemented in GameInfoManager, and a menu for managing registered plugins is implemented in GameInfoManagerMenu.

The core script provides functions for registering/unregistering listeners for pre-defined events, as well as registering delayed callbacks. The core script defines a single "change" event for whisper mode (stealth), with the main functionality being implemented in separate plugins. The plugins are designed to be autonymous and capable of being enabled/disabled individually to provide specific functionality, while being possible to disable to avoid hits to performance or incompatibility with game patches or other mods. Plugins can be enabled/disabled from the mod menu, or alternatively by removing them from the mod.txt. Loaded plugins will automatically be enabled if not manually disabled. Additional plugins may also be loaded from separate mods if needed.


The below information is mainly intended for developers to give a short overview of the main design. The HUDList script, being the most prominent user of this, provides several additional examples on possible uses.


Three functions defined by the core script are of primary interest: the listener registering, unregistering and signalling:

	GameInfoManager:register_listener(listener_id, source_type, event, clbk, keys, data_only)
		Registers a new listener. The source_type and event arguments define the type of event to listen to, while the the listener_id is an identifier for the listener that must be unique for any combination of source_type and event. The clbk argument specifies a function to call when the listener is signalled. The last two arguments are optional; keys is an array of identifiers to limit events to, and data_only is a boolean flagging if only the raw data should be included in the signal. If these are not provided, events are not filtered by the key values, and the signal will include the event and the key as well as the data.
	
	GameInfoManager:unregister_listener(listener_id, source_type, event)
		Unregisters an existing listener by its ID for a given source and event combination.
	
	GameInfoManager:_listener_callback(source, event, key, ...)
		Raises a signal to all listeners that are registered for the source/event combination, and possibly filtered by the key value if defined by the listener. If the listener has not set the data_only flag, the event and key is sent, with any varargs following as additional arguments.

As an example, see the GIM_Pager_Plugin. This plugin defines an event handler and hooks into several game scripts to gather data about the state of pagers in the game. One of the state changes for a pager is when the player interacts with it (i.e. answering it), which leads to the following call:

	managers.gameinfo:event("pager", "set_answered", tostring(self._active_unit:key()))

The GameInfoManager:event() function is a shorthand for handling specific types of events, done by deriving a callback name from the source provided as "_<source>_event". In this case, the source is "pager", resulting in the defined event handler "_pager_event" being called. "pager" is the source and "set_answered" is the event, while the interactive unit key is the event key. The event key is what allows the script to separate one particular pager from another.
The event handler that will receive this is:

	GameInfoManager:_pager_event(event, key, data)

The event and the key corresponds to the above with the source being filtered out at this stage. The data is nil here as no extra argument was provided in the event call after the key. The event handler defines three events: add, remove and set_answered, that each will react appropriately when raised by creating a new pager entry, removing it, or modifying it to flag it as answered. They will also make sure that any listeners are called to update them that a pager event has occured. For the set_answered event, this is done with the follow call:

	self:_listener_callback("pager", "set_answered", key, self._pagers[key])

Here the GIM is told to raise an event for the "pager" source type, with the event type "set_anwered", that this is an event for the object referenced by the key, and that it should also include the data that has been defined for this particular pager (the most relevant of which would be that the answered flag has been set). The first two arguments are not optional, while the key is recommended in most cases, and the data can be any number and types of arguments; it will be up to the listener to interpret the event correctly.

As an example of the listener implementation, the HUDList script uses several listeners for various events from different plugins, including for pagers. In HUDListClasses, and specifically in the HUDList.PagerItem class, a single listener is registered per pager that has been triggered. The pager list item, while the exact implementation differs somewhat, will use the following call to register a listener:

	managers.gameinfo:register_listener("HUDList_pager_listener_<key>", "pager", "set_answered", callback(self, self, "_set_answered"), { <key> }, true)

In order, it creates a unique listener name using the pager key, specifies that it wants to know about pagers being answered, provides a callback to call when the event is raised, limits the events to those concerning the pager associated with the specific key only, and finally flags that it is only interested in the data (in this case because it already knows what the event and the key will be when it receives an event). The callback function will be left to deal with the event appropriately (in this case by altering the HUD element a little), and its signature is the following:

	HUDList.PagerItem:_set_answered(pager_data)

In addition, the pager class will also unregister the listener when destroyed:

	managers.gameinfo:unregister_listener("HUDList_pager_listener_<key>", "pager", "set_answered")

The main HUDList script also contains a listener for pager events used for creating the pager items to begin with:

	managers.gameinfo:register_listener("HUDListManager_pager_listener", "pager", "add", callback(self, self, "_pager_event"))
	managers.gameinfo:register_listener("HUDListManager_pager_listener", "pager", "remove", callback(self, self, "_pager_event"))

Note here that the listener ID is not unique, but this is fine because the source/event combination differs. This listener listens to all pager events, not limited to specific ones by providing the relevant keys (would be 5th argument), and will also want to see the event/key when an event is raised by not setting the data_only flag (6th argument), which will trigger the following callback:

	HUDListManager:_pager_event(event, key, data)

Here it will either create or destroy the pager list item depending on the event type and pass on the key to the item that it will then use to create its own listener above. Note that as the data_only flag was not set, the event and key is included and the same callback function can be used to handle several different events for several different pagers.